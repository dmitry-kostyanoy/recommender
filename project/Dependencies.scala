import sbt._

object Dependencies {

  object akka {
    def apply(x: String) = {
      val version =
        if (x.startsWith("http")) "10.0.10"
        else "2.5.4"

      "com.typesafe.akka" %% s"akka-$x" % version
    }

    val core = apply("actor")
    val stream = apply("stream")

    val http = apply("http")
    val httpTestkit =  apply("http-testkit") % "test"
    val slf4j = apply("slf4j")
  }

  object cats {
    val core = "org.typelevel" %% "cats-core" % "0.9.0"
  }

  object circe {
    val version = "0.8.0"
    val core = "io.circe" %% "circe-core" % version
    val generic = "io.circe" %% "circe-generic" % version
    val parser = "io.circe" %% "circe-parser" % version % "test"
    val akkaHttp = "de.heikoseeberger" %% "akka-http-circe" % "1.18.0"
  }

  object log {
    val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"
    val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2"
  }

  object tests {
    val specs = "org.specs2" %% "specs2-core" % "3.9.5" % "test"
  }
}
