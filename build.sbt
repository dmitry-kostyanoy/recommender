import Dependencies._

lazy val root = (project in file(".")).
  settings(
    scalaVersion := "2.12.3",
    name := "Recommender",

    cancelable in Global := true,

    libraryDependencies ++= Seq(
      akka.core, akka.stream, akka.slf4j,
      akka.http, akka.httpTestkit,
      cats.core,
      circe.core, circe.generic, circe.parser, circe.akkaHttp,
      log.logback, log.scalaLogging,
      tests.specs
    )
  )
