package recommender.domain

import Recommender.Error._
import akka.actor.{Actor, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.specs2.mutable.SpecificationLike
import org.specs2.specification.{AfterAll, Scope}

class UserManagerSpec
  extends TestKit(ActorSystem("test-system"))
  with ImplicitSender
  with SpecificationLike
  with AfterAll {
  import UserHandler._
  import UserManager._

  sequential // needed for akka-testkit and specs2 to play nice together

  def afterAll(): Unit = shutdown()

  "User manager actor" >> {
    "forwards register requests to the newly created child" >> new TestScope {
      val manager = system.actorOf(UserManager.props(makeChild))
      manager ! Register(UserInfo("userName", Email("email@email.com"), 32, Gender.Male))

      probe.expectMsg(GetInitialRecommendation)
      probe.reply("reply from child")
      expectMsg("reply from child")
      ok
    }

    "forwards action requests to the previously created child" >> new TestScope {
      val generateUserId: () => UserId = () => UserId(111)
      val manager = system.actorOf(UserManager.props(makeChild, generateUserId))

      manager ! Register(UserInfo("userName", Email("email@email.com"), 32, Gender.Male))
      probe.expectMsg(GetInitialRecommendation)

      manager ! RespondToAction(UserAction(UserId(111), VideoId(222), Action.Like))
      probe.expectMsg(RecommendNext(VideoId(222), Action.Like))
      probe.reply("reply from child")
      expectMsg("reply from child")
      ok
    }

    "responds with an error when receiving an action for an unknown user" >> new TestScope {
      val manager = system.actorOf(UserManager.props())
      manager ! RespondToAction(UserAction(UserId(111), VideoId(222), Action.Like))

      probe.expectNoMsg()
      expectMsg(UserDoesNotExist)
      ok
    }
  }

  trait TestScope extends Scope {
    val probe = TestProbe()
    val makeChild: UserId => Props = _ => Props(new Actor {
      def receive: Receive = {
        case x => probe.ref.forward(x)
      }
    })
  }
}
