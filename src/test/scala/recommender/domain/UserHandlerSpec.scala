package recommender.domain

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import org.specs2.mutable.SpecificationLike
import org.specs2.specification.AfterAll
import recommender.domain.Recommender.Error.VideoDoesNotMatchLast

class UserHandlerSpec
  extends TestKit(ActorSystem("test-system"))
  with ImplicitSender
  with SpecificationLike
  with AfterAll {
  import UserHandler._

  sequential

  def afterAll(): Unit = shutdown()

  "User handler actor" >> {
    val userId = UserId(111)

    "responds with a recommendation to the initial request" >> {
      val videoId = VideoId(222)
      val actor = system.actorOf(UserHandler.props(userId, Set(videoId)))

      actor ! GetInitialRecommendation
      expectMsg(Recommendation(userId, videoId))
      ok
    }

    "responds with the least viewed video to the subsequent requests" >> {
      val actor = TestActorRef[UserHandler](
        UserHandler.props(userId, Set(VideoId(222), VideoId(333))))

      actor ! GetInitialRecommendation
      val first = expectMsgType[Recommendation]
      actor.underlyingActor.lastRecommended mustEqual first.videoId

      actor ! RecommendNext(first.videoId, Action.Like)
      val second = expectMsgType[Recommendation]
      actor.underlyingActor.lastRecommended mustEqual second.videoId
      actor.underlyingActor.viewMap mustEqual Map(first.videoId -> 1, second.videoId -> 0)

      first.videoId mustNotEqual second.videoId
    }

    "responds with an error if action videoId is not the last recommended" >> {
      val actor = system.actorOf(
        UserHandler.props(userId, Set(VideoId(222), VideoId(333))))

      actor ! GetInitialRecommendation
      expectMsgType[Recommendation]

      actor ! RecommendNext(VideoId(555), Action.Like)
      expectMsg(VideoDoesNotMatchLast)
      ok
    }

  }
}
