package recommender.api

import akka.http.scaladsl.server.{Rejection, RejectionHandler}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import cats.data.NonEmptyList
import cats.syntax.show._
import recommender.domain.Recommender
import io.circe.generic.auto._
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport._
import recommender.api.Responses.ErrorResponse
import recommender.api.validation.ValidationError

object Rejections extends ShowInstances {

  case class ValidationRejection(errors: NonEmptyList[ValidationError]) extends Rejection
  case class RecommendationRejection(error: Recommender.Error) extends Rejection

  val Handler: RejectionHandler =
    RejectionHandler.newBuilder()
      .handle {
        case ValidationRejection(errors) =>
          complete((BadRequest, ErrorResponse(errors.toList.map(_.show))))
      }
      .handle {
        case RecommendationRejection(error) =>
          complete((BadRequest, ErrorResponse(List(error.show))))
      }
      .result()
  
}
