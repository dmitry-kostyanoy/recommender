package recommender.api.validation

import cats.data.Validated.invalidNel
import cats.data.ValidatedNel
import cats.implicits._
import recommender.api.Requests._
import recommender.domain.Action._
import recommender.domain.Gender._
import recommender.domain._

sealed trait ValidationError

object ValidationError {
  case object EmailIsInvalid extends ValidationError
  case object AgeIsInvalid extends ValidationError
  case object GenderIsInvalid extends ValidationError
  case object ActionIsInvalid extends ValidationError
}

object RequestValidation {
  import ValidationError._

  private type Result[T] = ValidatedNel[ValidationError, T]

  private val EmailRegex =
    """^[a-zA-Z0-9\.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$""".stripMargin.r

  private[validation] def validate(rr: RegisterRequest): Result[UserInfo] = {
    val email: Result[Email] =
      EmailRegex.findFirstIn(rr.email).toValidNel(EmailIsInvalid).map(Email.apply)

    val age: Result[Age] =
      Some(rr.age).filter(age => age >= 5 && age <= 120).toValidNel(AgeIsInvalid)

    val gender: Result[Gender] = rr.gender match {
      case 1 => Male.validNel
      case 2 => Female.validNel
      case _ => invalidNel(GenderIsInvalid)
    }

    (email |@| age |@| gender).map { case (e, a, g) => UserInfo(rr.userName, e, a, g) }
  }

  private[validation] def validate(ar: ActionRequest): Result[UserAction] = {
    val action: Result[Action] =
      ar.actionId match {
        case 1 => Like.validNel
        case 2 => Skip.validNel
        case 3 => Play.validNel
        case _ => invalidNel(ActionIsInvalid)
      }

    action.map(a => UserAction(UserId(ar.userId), VideoId(ar.videoId), a))
  }

  trait Validator[T, R] {
    def validate(t: T): Result[R]
  }

  implicit val registerRequestValidator: Validator[RegisterRequest, UserInfo] =
    (t: RegisterRequest) => validate(t)

  implicit val actionRequestValidator: Validator[ActionRequest, UserAction] =
    (t: ActionRequest) => validate(t)
}
