package recommender.api

import cats.Show
import cats.Show._
import io.circe.Encoder
import recommender.api.validation.ValidationError
import recommender.api.validation.ValidationError._
import recommender.domain.{Recommender, UserId, VideoId}
import recommender.domain.Recommender.Error._

trait ShowInstances {

  implicit val showValidationError: Show[ValidationError] =
    show {
      case EmailIsInvalid => "email is not valid"
      case AgeIsInvalid => "age is not valid"
      case GenderIsInvalid => "gender is not valid"
      case ActionIsInvalid => "action is not valid"
    }

  implicit val showRecServiceError: Show[Recommender.Error] =
    show {
      case UserDoesNotExist => "userId does not exist"
      case VideoDoesNotMatchLast => "video does not correspond to last given"
    }
}

trait JsonFormats {
  implicit val encodeUserId: Encoder[UserId] = Encoder.encodeLong.contramap(_.value)
  implicit val encodeVideoId: Encoder[VideoId] = Encoder.encodeLong.contramap(_.value)
}
