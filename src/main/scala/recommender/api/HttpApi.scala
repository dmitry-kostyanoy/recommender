package recommender.api

import akka.http.scaladsl.server.Directives.{validate => _, _}
import akka.http.scaladsl.server.Route
import cats.data.Validated.{Invalid, Valid}
import recommender.api.Requests._
import recommender.api.validation.RequestValidation._
import recommender.domain.Recommender
import io.circe.generic.auto._
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport._
import recommender.api.Rejections.{RecommendationRejection, ValidationRejection}

class HttpApi(recommender: Recommender) extends JsonFormats {

  def route: Route =
    post {
      path("register") {
        entity(as[RegisterRequest]) { req =>
          validated(req)(recommender.register)
        }
      } ~
      path("action") {
        entity(as[ActionRequest]) { req =>
          validated(req)(recommender.handleAction)
        }
      }
    }

  private def validated[T, R](t: T)(f: R => Recommender.Result)(implicit V: Validator[T, R]) =
    V.validate(t) match {
      case Valid(r) => onSuccess(f(r)) {
        case Left(error) => reject(RecommendationRejection(error))
        case Right(recommendation) => complete(recommendation)
      }
      case Invalid(ee) => reject(ValidationRejection(ee))
    }
}
