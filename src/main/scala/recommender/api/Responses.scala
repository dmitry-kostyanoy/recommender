package recommender.api

import scala.collection.immutable._

object Responses {
  case class OkResponse(userId: Long, videoId: Long)
  case class ErrorResponse(errors: Iterable[String])
}
