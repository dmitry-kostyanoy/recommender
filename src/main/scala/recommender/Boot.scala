package recommender

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration.Duration
import scala.io.StdIn

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.RejectionHandler
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import recommender.api.{HttpApi, Rejections}
import recommender.domain._

object Boot extends App {
  val serviceConfig = ConfigFactory.load().getConfig("service")
  val httpConfig = serviceConfig.getConfig("http")

  implicit val actorSystem: ActorSystem = ActorSystem("recommender")
  implicit val ec: ExecutionContextExecutor = actorSystem.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val recommender = {
    val timeoutDuration = serviceConfig.getDuration("recommender-actor-timeout")
    implicit val timeout = Timeout(Duration.fromNanos(timeoutDuration.toNanos))

    val userManager = actorSystem.actorOf(UserManager.props())
    new ActorBasedRecommender(userManager)
  }

  val route = new HttpApi(recommender).route
  implicit val rejectionHandler: RejectionHandler = Rejections.Handler

  val bindingFuture =
    Http().bindAndHandle(
      route,
      httpConfig.getString("host"),
      httpConfig.getInt("port")
    )

  println(s"Server is online\nPress RETURN to stop...")
  StdIn.readLine()

  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => actorSystem.terminate())
}
