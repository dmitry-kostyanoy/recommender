package recommender.domain

import scala.concurrent.ExecutionContext

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import Recommender.{Error => RecommenderError}

class ActorBasedRecommender(
  userManager: ActorRef
)(implicit timeout: Timeout, ec: ExecutionContext) extends Recommender {
  import recommender.domain.Recommender.Result
  import UserManager._

  private val translateActorResponse: Any => Either[Recommender.Error, Recommendation] = {
    case r: Recommendation => Right(r)
    case error: Recommender.Error => Left(error)
    case event =>
      throw new RuntimeException(s"got an unexpected event $event from the UserManager")
  }

  def register(user: UserInfo): Result =
    (userManager ? Register(user)).map(translateActorResponse)

  def handleAction(action: UserAction): Result =
    (userManager ? RespondToAction(action)).map(translateActorResponse)
}

class UserManager(
  childProps: UserId => Props,
  generateUserId: () => UserId
) extends Actor with ActorLogging {
  import UserManager._
  import UserHandler._

  private def makeChild(): ActorRef = {
    val userId = generateUserId()
    context.actorOf(childProps(userId), userId.toString)
  }

  private def findChild(userId: UserId): Option[ActorRef] =
    context.child(userId.toString)

  def receive: Receive = {
    case Register(userInfo) =>
      log.info("register {}", userInfo)

      val childActor = makeChild()
      childActor.forward(GetInitialRecommendation)

    case RespondToAction(userAction) =>
      findChild(userAction.userId) match {
        case Some(childActor) =>
          log.debug("forwarding {} to {}", userAction, childActor)
          childActor.forward(RecommendNext(userAction.videoId, userAction.action))
        case None =>
          log.warning("user {} does not exist", userAction.userId)
          sender() ! RecommenderError.UserDoesNotExist
      }
  }
}

object UserManager {
  case class Register(userInfo: UserInfo)
  case class RespondToAction(userAction: UserAction)

  private def randomPositiveLong: Long = {
    val range = 10000000L
    val number = scala.math.random()
    (number * range).toLong
  }

  private def makeVideoList: Set[VideoId] =
    Stream.continually(randomPositiveLong)
      .distinct
      .take(10)
      .map(VideoId.apply)
      .toSet

  private def defaultMakeChild(userId: UserId): Props =
    UserHandler.props(userId, makeVideoList)

  private def generateRandomUserId: UserId =
    UserId(randomPositiveLong)

  def props(
    makeChildProps: UserId => Props = defaultMakeChild,
    generateUserId: () => UserId = () => generateRandomUserId
  ): Props = Props(new UserManager(makeChildProps, generateUserId))
}

class UserHandler(
  userId: UserId,
  videos: Set[VideoId]
) extends Actor with ActorLogging {
  import UserHandler._

  private[domain] var viewMap: Map[VideoId, Int] = {
    val viewCounts = List.fill(videos.size)(0)
    videos.zip(viewCounts).toMap
  }

  private[domain] var lastRecommended: VideoId = _

  private def leastViewed: VideoId = {
    log.debug("current viewMap {}", viewMap)
    viewMap.toList.minBy(_._2)._1
  }

  private def nextVideo(): VideoId = {
    val next = leastViewed
    lastRecommended = next
    next
  }

  private def incrementViewCount(videoId: VideoId) = {
    val currentCount = viewMap(videoId)
    viewMap = viewMap.updated(videoId, currentCount + 1)
  }

  def receive: Receive = {
    case GetInitialRecommendation =>
      val next = nextVideo()
      log.debug("initially recommending {} to {}", next, userId)
      sender() ! Recommendation(userId, next)

    case RecommendNext(videoId, action) =>
      if (videoId != lastRecommended) {
        sender() ! RecommenderError.VideoDoesNotMatchLast
      } else {
        log.info("user {} reacted with {} to {}", userId, action, videoId)

        incrementViewCount(videoId)
        val next = nextVideo()
        log.debug("recommending {} to {}", next, userId)

        sender() ! Recommendation(userId, next)
      }
  }
}

object UserHandler {
  case object GetInitialRecommendation
  case class RecommendNext(videoId: VideoId, action: Action)

  def props(userId: UserId, videos: Set[VideoId]): Props =
    Props(new UserHandler(userId, videos))
}
