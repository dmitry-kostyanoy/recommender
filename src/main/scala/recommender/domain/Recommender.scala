package recommender.domain

import scala.concurrent.Future

import Recommender._

trait Recommender {
  def register(user: UserInfo): Result
  def handleAction(action: UserAction): Result
}

object Recommender {
  sealed trait Error
  object Error {
    case object UserDoesNotExist extends Error
    case object VideoDoesNotMatchLast extends Error
  }

  type Result = Future[Either[Error, Recommendation]]
}
